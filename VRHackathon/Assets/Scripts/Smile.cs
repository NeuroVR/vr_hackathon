using UnityEngine;

public class Smile : MonoBehaviour
{

    private OVRFaceExpressions faceExpressions;
    public GameObject Sphere;

    private void Start()
    {
        // Get the OVRFaceExpressions component from the GameObject
        faceExpressions = GetComponent<OVRFaceExpressions>();
        if (faceExpressions == null)
        {
            Debug.LogError("OVRFaceExpressions component not found!");
            enabled = false; // Disable this script if OVRFaceExpressions is not found

        }

    }

    private void Update()
    {
        if (faceExpressions.ValidExpressions)
        {

            float leftLipCornerWeight = faceExpressions.GetWeight(OVRFaceExpressions.FaceExpression.LipCornerPullerL);
            float rightLipCornerWeight = faceExpressions.GetWeight(OVRFaceExpressions.FaceExpression.LipCornerPullerR);


            if (Sphere.GetComponent<Sphere>().IsFixating==true)
            {
                float Smile = leftLipCornerWeight + rightLipCornerWeight;
                Sphere.GetComponent<Sphere>().SetSize(Smile);
            }


            if (leftLipCornerWeight > 0.5f && rightLipCornerWeight > 0.5f)
            {
                //TriggerActionC();
            }
            else if (leftLipCornerWeight >= 0.5f && rightLipCornerWeight <= 0.5f)
            {
                //TriggerActionA();
            }
            else if (rightLipCornerWeight >= 0.5f && leftLipCornerWeight <= 0.5f)
            {
                //TriggerActionB();
            }
        }
    }

    private void TriggerActionA()
    {

        Debug.Log("Action A triggered!");
    }

    private void TriggerActionB()
    {

        Debug.Log("Action B triggered!");
    }

    private void TriggerActionC()
    {

        Debug.Log("Action C triggered!");
    }
}
