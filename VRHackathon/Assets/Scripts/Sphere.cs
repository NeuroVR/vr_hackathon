using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere : MonoBehaviour
{
    private Material material;
    public bool IsFixating;


    private void Start()
    {
        material = GetComponent<Renderer>().material;
    }

    public void ChangeToRed()
    {
        material.color = Color.red;

    }
    public void ChangeToWhite()
    {
        material.color = Color.white;

    }
    void Update()
    {
       
    }



    public void SetSize(float multiplier)
    {

        multiplier = 0.15f + multiplier * 0.3f;
        transform.localScale = new Vector3(multiplier, multiplier, multiplier);


    }



}
