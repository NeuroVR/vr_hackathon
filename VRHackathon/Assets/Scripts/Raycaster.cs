using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycaster : MonoBehaviour
{
    public float raycastDistance = 100f;
    private Transform lastHitObject;


    void Update()
    {
        Debug.DrawRay(transform.position, transform.forward * raycastDistance, Color.blue);

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, raycastDistance))
        {
            if (hit.transform.name == "Sphere" && lastHitObject != hit.transform)
            {
                // A new object is hit by the raycast
                if (lastHitObject != null && lastHitObject.name == "Sphere")
                {
                    lastHitObject.GetComponent<Sphere>().ChangeToWhite();
                }
                hit.transform.GetComponent<Sphere>().ChangeToRed();
                lastHitObject = hit.transform;
                lastHitObject.GetComponent<Sphere>().IsFixating = true;
            }
        }
        else if (lastHitObject != null && lastHitObject.name == "Sphere")
        {
            // Raycast is no longer hitting the sphere
            lastHitObject.GetComponent<Sphere>().ChangeToWhite();
            lastHitObject.GetComponent<Sphere>().IsFixating = false;
            lastHitObject = null;
        }
    }
}
