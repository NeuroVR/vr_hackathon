# Eye & Face Tracking with Meta Quest Pro in Unity

This Unity project demonstrates a minimalistic approach to use eye and face tracking with the Meta Quest Pro  to interact with the environment

## Features

- **Eye Tracking**: When the sphere is fixated upon with either the left or right eye, its color changes from white to red. It reverts back to white when not fixated upon.
  
- **Face Tracking**: While the sphere is red (i.e., fixated upon), the user's smile controls its scale. The broader the smile, the larger the sphere becomes.

